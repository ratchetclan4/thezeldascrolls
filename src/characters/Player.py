import pygame
from Character import *

class Player(Character):
	def __init__(self, sprite_dict, image_tile_filename, image_tile_size):
		super(Player, self).__init__(sprite_dict, image_tile_filename, image_tile_size)
		self.old_x = self.rect.x
		self.old_y = self.rect.y
		self.new_tile = False
		self.fasttravel_start = None
		self.fasttravel_end = None

	# add a quest to log
	def add_quest(self, label, quest):
		if self.sprite_dict['Quests'].contains(label):
			pass
		else:
			self.sprite_dict['Quests'].add(label, quest)

	# remove quest from log
	def remove_quest(self, label):
		self.sprite_dict['Quests'].remove(label)

	# use item from inventory
	def use(self, label):
		item = self.sprite_dict['Inventory'].remove(label)
		self.sprite_dict[item.attribute] += item.value

	# cast spell from spell list
	def use_spell(self, label):
		item = self.sprite_dict['Spells'].remove(label)
		self.sprite_dict[item.attribute] -= item.value

    # buy item and add to container
	def buy(self, label, item):
		if self.gold() + item.value >= 0: 
			self.sprite_dict['Gold'] -= item.value
			self.sprite_dict[item.container].add(label, item)

	# sell item and remove from container
	def sell(self, label):
		self.sprite_dict['Gold'] += 1
		item = self.sprite_dict['Inventory'].remove(label)
		if item == None:
			item = self.sprite_dict['Spells'].remove(label)
		return item

	# add city to log
	def add_city(self, label, location):
		if self.sprite_dict['Cities'].contains(label):
			pass
		else:
			self.sprite_dict['Cities'].add(label, location)

	# return list of quests in quest log
	def quests(self):
		return list(self.sprite_dict['Quests'].keys())

	# return list of items in inventory
	def inventory(self):
		return list(self.sprite_dict['Inventory'].keys())

	# return list of spells
	def spells(self):
		return list(self.sprite_dict['Spells'].keys())

	# return list of cities
	def cities(self):
		return list(self.sprite_dict['Cities'].keys())

	# fast travel from start city to end city
	def fasttravel(self, start, end):
		self.fasttravel_start = self.sprite_dict['Cities'].get_item(start)
		self.fasttravel_end = self.sprite_dict['Cities'].get_item(end)
		print "hello"

	# earn xp 
	def earn_xp(self, xp):
		self.sprite_dict['XP'] += xp
		if self.xp() >= self.max_xp():
			self.sprite_dict['XP'] = self.xp() - self.max_xp()
			self.sprite_dict['Points'] += 1
			self.sprite_dict['Level'] += 1 

	# earn gold
	def earn_gold(self, gold):
		self.sprite_dict['Gold'] += gold

	# gold
	def gold(self):
		return self.sprite_dict['Gold']

	# level
	def level(self):
		return self.sprite_dict['Level']

	# points
	def points(self):
		return self.sprite_dict['Points']

	# level up sprite attribute
	def levelup(self, attribute):
		if self.points() > 0:
			self.sprite_dict[attribute] += 1
			self.sprite_dict['Points'] -= 1

	# return true if player can level up, false otherwise
	def isLeveled(self):
		if self.points() > 0:
			return True
		else:
			return False

	# updates player quests
	# def 

	# set player coords back to previous player coords
	def reset_coordinates(self):
		self.rect.x = self.old_x
		self.rect.y = self.old_y

	# set/change flags dictating player animation in shops
	def animate_shop(self, flag):
		if flag == "enter":
   			self.reposition((200,200))
   			flag = "walk right"
		elif flag == "leave":
			flag = "walk left"
		elif flag == "walk right":
			self.redirect('right')
			if self.rect.x < 384:
				self.x_vel = self.velocities['right'][0]
			else:
				self.direction = 'rest'
				self.x_vel = 0
		elif flag == "walk left":
			self.redirect('left')
			if self.rect.x > 200:
  				self.x_vel = self.velocities['left'][0]
			else:
				self.reposition((self.old_x, self.old_y))
				flag = "exit shop"
				self.direction = 'rest'
				self.state = 'resting'
				self.x_vel = 0

		self.rect.x += self.x_vel
		self.animation()
		self.rescale((128, 128))
		return flag

	# set/change flags dictating player animation in battle
	def animate_battle(self, flag):
		if flag == "enter":
			self.reposition((50, 200))
			flag = "walk right"
		elif flag == "leave":
			flag = 'walk left'
		elif flag == "walk right":
			self.redirect('right')
			if self.rect.x < 200:
				self.x_vel = self.velocities['right'][0]
			else:
				self.direction = 'rest'
				self.x_vel = 0
		elif flag == 'walk left':
			self.redirect('left')
			if self.rect.x > 60:
				self.x_vel = self.velocities['left'][0]
			else:
				self.reposition((self.old_x, self.old_y))
				flag = "exit battle"
				self.direction = 'rest'
				self.state = 'resting'
				self.x_vel = 0
		elif flag == "start attack":
			self.redirect('right')
			if self.rect.x < 250:
				self.x_vel = self.velocities['right'][0]
			else:
				flag = "end attack"
		elif flag == "end attack":
			if self.rect.x > 200:
				self.x_vel = self.velocities['left'][0]
			else:
				self.direction = 'rest'
				self.x_vel = 0
				flag = "enemy start attack"

		if self.sprite_dict['Health'] == 0:
			flag = "player died"

		self.rect.x += self.x_vel
		self.animation()
		self.rescale((128, 128))
		return flag

	# set/change flags dictating player animation in stat screen
	def animate_stats(self, flag):
		if flag == "leave":
			self.reposition((self.old_x, self.old_y))
			flag = "exit"
		elif flag == "enter":
			self.reposition((0, 0))
			flag = ""

		self.animation()
		self.rescale((256, 256))
		return flag

	def handle_event(self, event):
		if event != None:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT:
					self.begin_moving('left')
				elif event.key == pygame.K_RIGHT:
					self.begin_moving('right')
				elif event.key == pygame.K_UP:
					self.begin_moving('up')
				elif event.key == pygame.K_DOWN:
					self.begin_moving('down')

			elif event.type == pygame.KEYUP:
				self.begin_resting(self.direction)


	def handle_state(self):
		if self.state == 'resting':
			self.begin_resting(self.direction)
		elif self.state == 'moving':
			self.auto_moving(self.direction)


	def update(self, event):
		self.handle_event(event)
		self.handle_state()
		self.animation()

		self.rect.x += self.x_vel
		self.rect.y += self.y_vel

		# print "rect.x: " + str(self.rect.x)
		# print "rect.y: " + str(self.rect.y)
		# print self.old_x
		# print self.old_y




		# 8672 10752 carathas
		# 7808 12352 darkstream
		# 9440 8864 felguard
		# 7232 7296 winterhelm