import pygame
from random import randint
from constants import *
from colors import *
from tools import *
from Component import *


class Character(pygame.sprite.Sprite):
	def __init__(self, sprite_dict, image_tile_filename, image_tile_size):
		super(Character, self).__init__()
		self.sprite_dict = sprite_dict
		self.image_tile_size = image_tile_size
		self.image_tile = SpriteSheet(image_tile_filename)
		self.image = self.image_tile.image_at((0, 0, 32, 32))
		self.image_dict = {}
		self.image_index = 0
		self.frame = 1
		self.screen_rect = self.image.get_rect()
		self.screen_rect.x = self.sprite_dict['Location'][0]
		self.screen_rect.y = self.sprite_dict['Location'][1]
		self.rect = self.image.get_rect()
		self.rect.x = self.sprite_dict['Location'][0]
		self.rect.y = self.sprite_dict['Location'][1]
		self.speed = 1
		self.x_vel = 0
		self.y_vel = 0
		self.state = 'resting'  # can be moving or resting
		self.direction = 'rest'  # direction of movement, i.e. left, right, up, down or at rest
		self.velocities = {'left': (-1,0), 'right': (1,0), 'up': (0,-1), 'down': (0,1)}
		self.make_image_dictionary()

	# make dictionary of images belonging to character sprite
	def make_image_dictionary(self):
		down = []; up = []; left = []; right = []; rest = []; attack = []
		for i in range(0, self.image_tile_size[0]):
			if self.image_tile_size[1] >= 4:
				down.append(self.image_tile.image_at((32*i, 0, 32, 32)))
				left.append(self.image_tile.image_at((32*i, 32, 32, 32)))
				right.append(self.image_tile.image_at((32*i, 64, 32, 32)))
				up.append(self.image_tile.image_at((32*i, 96, 32, 32)))
				rest.append(self.image_tile.image_at((0, 0, 32, 32)))
				attack.append(self.image_tile.image_at((0, 0, 32, 32)))

			if self.image_tile_size[1] >= 5:
				rest.append(self.image_tile.image_at((32*i, 128, 32, 32)))
				attack.append(self.image_tile.image_at((0, 0, 32, 32)))

			if self.image_tile_size[1] == 6:
				attack.append(self.image_tile.image_at((32*i, 160, 32, 32)))

		self.image_dict = {'down': down, 'up': up, 'left': left, 'right': right, 'rest': rest, 'attack': attack}

	# maximum health, mana, and xp of sprite 
	def max_health(self):
		return self.sprite_dict['Constitution']*10

	def max_mana(self):
		return self.sprite_dict['Intelligence']*10

	def max_xp(self):
		return self.sprite_dict['Level']*10

	# current health, mana, and xp of sprite 
	def health(self):
		return self.sprite_dict['Health']

	def mana(self):
		return self.sprite_dict['Mana']

	def xp(self):
		return self.sprite_dict['XP']

	# health, mana, and xp of sprite returned as a fraction of total between 0 and 1
	def health_fraction(self):
		return self.health() / self.max_health()

	def mana_fraction(self):
		return self.mana() / self.max_mana()

	def xp_fraction(self):
		return self.xp() / self.max_xp()

	# character attributes
	def strength(self):
		return self.sprite_dict['Strength']

	def agility(self):
		return self.sprite_dict['Agility']

	def constitution(self):
		return self.sprite_dict['Constitution']

	def wisdom(self):
		return self.sprite_dict['Wisdom']

	def intelligence(self):
		return self.sprite_dict['Intelligence']

	# sprite weapon base attack
	def attack(self):
		return self.sprite_dict['Weapon'].att

	# sprite weapon armour penetration
	def armour_penetration(self):
		return self.sprite_dict['Weapon'].ap

	# sprite armour value
	def armour(self):
		return self.sprite_dict['Armour'].armour

	# sprite position
	def position(self):
		return (self.old_x, self.old_y)

	# take physical damage from another sprite
	def take_physical_damage(self, sprite):
		sprite_weapon_attack = sprite.attack()
		sprite_weapon_ap = sprite.armour_penetration()
		sprite_strength = sprite.strength()
		sprite_agility = sprite.agility()

		armour = self.armour()

		damage = sprite_weapon_attack + sprite_strength
		damage -= armour - sprite_weapon_ap

		if damage < 0:
			damage = 0

		self.sprite_dict['Health'] -= 8*damage
		if self.sprite_dict['Health'] < 0:
			self.sprite_dict['Health'] = 0

	# take magical damage from another sprite
	def take_magical_damage(self, sprite):
		pass

	# return whether sprite is dead or alive
	def isDead(self):
		dead = False
		if self.sprite_dict['Health'] <= 0:
			dead = True
		return dead

	# methods to reposition, rescale, redirect, and restate character sprite
	def reposition(self, location):
		self.rect.x = location[0]
		self.rect.y = location[1]
		self.image_index = 0

	def rescale(self, scale):
		self.image = pygame.transform.scale(self.image, scale)

	def redirect(self, direction):
		self.direction = direction

	def restate(self, state):
		self.state = state

	# determine if input character is beside character
	def beside(self, character):
		if abs(self.rect.x - character.rect.x) + abs(self.rect.y - character.rect.y) == 32:
			return True
		else:
			return False

	# changes current character sprite image based on character state and movement direction
	def animation(self):
		if self.state == 'resting' and self.direction == 'rest':
			self.image = self.image_dict[self.direction][0]
			self.image_index = 0
			self.frame = 1
		else:
			if self.frame == 1:
				self.image = self.image_dict[self.direction][self.image_index]
				self.image_index = (self.image_index+1) % self.image_tile_size[0]
			elif self.frame == FPS/self.image_tile_size[0]:
				self.frame = 0
			self.frame += 1

	# methods controlling movement of character
	def begin_moving(self, direction):
		if self.rect.x % 32 == 0 and self.rect.y % 32 == 0:
			self.state = 'moving'
			self.direction = direction
			self.x_vel = self.velocities[direction][0]
			self.y_vel = self.velocities[direction][1]


	def auto_moving(self, direction):
		self.x_vel = self.velocities[direction][0]
		self.y_vel = self.velocities[direction][1]

		self.new_tile = False
		if self.rect.x % 32 == 0 and self.rect.y % 32 ==0:
			if self.old_x != self.rect.x or self.old_y != self.rect.y:
				self.new_tile = True
				self.old_x = self.rect.x
				self.old_y = self.rect.y


	def begin_resting(self, direction):
		self.state = 'resting'
		if self.rect.x % 32 == 0 and self.rect.y % 32 == 0:
			self.direction = 'rest'
			self.auto_resting()
		else:
			self.auto_moving(direction)


	def auto_resting(self):
		self.x_vel = 0
		self.y_vel = 0

		self.new_tile = False
		if self.rect.x % 32 == 0 and self.rect.y % 32 ==0:
			if self.old_x != self.rect.x or self.old_y != self.rect.y:
				self.new_tile = True
				self.old_x = self.rect.x
				self.old_y = self.rect.y


	# update character
	def update(self, event):
		pass

	# draw sprite at current position
	def draw(self, display, shift = (0, 0)):
		display.blit(self.image, (self.rect.x - shift[0], self.rect.y - shift[1]))

		# print self.rect.x 
		# print self.rect.y 
		# print self.old_x
		# print self.old_y