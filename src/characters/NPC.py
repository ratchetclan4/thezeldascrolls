import pygame
import random
from random import randint
from colors import *
from tools import *
from Dialog import *
from Quest import *
from Character import *


class NPC(Character):
	def __init__(self, sprite_dict, image_tile_filename, image_tile_size, box = None):
		super(NPC, self).__init__(sprite_dict, image_tile_filename, image_tile_size)
		self.old_x = self.rect.x
		self.old_y = self.rect.y
		self.new_tile = False
		self.npc_paused = True
		self.npc_moving = False
		self.npc_talking = False
		self.bounding_box = box 
		self.tiles_moved = 0
		self.frames_paused = 0

	# return NPC xml_tree 
	def xml_tree(self):
		# print self.sprite_dict['Dialog'].root[0]
		# return self.sprite_dict['Dialog'].root[0]
		return self.sprite_dict['Dialog'].get_tree()

	def handle_random_walk(self):
		if self.npc_paused:
			if self.frames_paused == 200:
				self.npc_paused = False
				self.npc_moving = True
				self.frames_paused = 0
				rand = random.randint(1,4)
				rand = self.check_bounding_box(rand)
				if rand == 1:
					self.begin_moving('left')
				elif rand == 2:
					self.begin_moving('right')
				elif rand == 3:
					self.begin_moving('up')
				else:
					self.begin_moving('down')
			else:
				if self.npc_talking == False:
					self.frames_paused += 1

		elif self.npc_moving:
			if self.tiles_moved == 1:
				self.npc_paused = True
				self.npc_moving = False
				self.tiles_moved = 0
				self.begin_resting(self.direction)
			else:
				if self.new_tile == True:
					self.tiles_moved += 1

	def handle_state(self):
		if self.state == 'resting':
			self.begin_resting(self.direction)
		elif self.state == 'moving':
			self.auto_moving(self.direction)


	def update(self, event):
		self.handle_random_walk()
		self.handle_state()
		self.animation()

		self.rect.x += self.x_vel
		self.rect.y += self.y_vel

	def stop_walk(self):
		self.rect.x = self.old_x
		self.rect.y = self.old_y


	def check_bounding_box(self, n):
		future_x = self.rect.x
		future_y = self.rect.y
		if n == 1:
			self.future_x = self.rect.x - 32*2
		elif n ==2:
			self.future_x = self.rect.x + 32*2
		elif n == 3:
			self.future_y = self.rect.y - 32*2
		else:
			self.future_y = self.rect.y + 32*2

		direction = n
		if n == 1:
			if self.future_x <= self.bounding_box.x:
				direction = 2
		elif n == 2:
			if self.future_x >= self.bounding_box.x + self.bounding_box.width:
				direction = 1
		elif n == 3:
			if self.future_y <= self.bounding_box.y:
				direction = 4
		elif n == 4:
			if self.future_y >= self.bounding_box.y + self.bounding_box.height:
				direction = 3

		return direction

	def generate_quest(self):
		return TalkToNPC('Steve')