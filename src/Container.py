import pygame


class Container(object):
	def __init__(self):
		self.item_dict = {}
		self.quantity_dict = {}

	# adds an item given by label to the container
	def add(self, label, item):
		self.item_dict[label] = item
		if label in self.quantity_dict:
			self.quantity_dict[label] += 1
		else:
			self.quantity_dict[label] = 1

	# removes and returns an item given by label from the container
	def remove(self, label):
		item = None
		if label in self.quantity_dict:
			if self.quantity_dict[label] == 1:
				quantity = self.quantity_dict.pop(label, None)
				item = self.item_dict.pop(label, None)
			else:
				self.quantity_dict[label] -= 1
				item = self.item_dict[label]

		return item

	# returns the item given by label
	def get_item(self, label):
		item = None
		if label in self.item_dict:
			item = self.item_dict[label]
		return item

	# empties the container
	def empty(self):
		self.item_dict = {}
		self.quantity_dict = {}

	# returns the quantity of the item given by label
	def quantity(self, label):
		count = 0
		if label in self.item_dict:
			count = self.quantity_dict[label]
		return count

	# returns True if the given label is in the container, else returns False
	def contains(self, label):
		if label in self.item_dict:
			return True
		else:
			return False

	# returns quantity_dict keys
	def keys(self):
		return list(self.quantity_dict.keys())