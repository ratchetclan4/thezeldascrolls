import pygame

pygame.font.init()

# fonts
# small_font = pygame.font.SysFont("comicsansms",25)
# medium_font = pygame.font.SysFont("comicsansms",50)
# large_font = pygame.font.SysFont("comicsansms",75)


small_font = pygame.font.SysFont("freesans",25)
medium_font = pygame.font.SysFont("freesans",50)
large_font = pygame.font.SysFont("freesans",75)