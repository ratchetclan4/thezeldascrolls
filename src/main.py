import pygame
from setup import *
from Game import *


def main():
	game_instance = Game()
	game_instance.set_states(game_states)
	game_instance.game_loop()
