import pygame
import setup
from constants import *
from tools import *
from Menu import *
from DialogBox import *
# from Bar import *
from State import *


class Stats(State):
	def __init__(self, stats_characters, stats_components, prev, curr, next):
		super(Stats, self).__init__(stats_characters, stats_components, prev, curr, next)
		self.stats_image = SpriteSheet("../artwork/battle.png").get_image() 
		self.stats_image = pygame.transform.scale(self.stats_image, (SCREEN_SIZE[0], SCREEN_SIZE[1]))
		self.player_stats_list = None
		self.player_inventory_list = None
		self.player_spells_list = None

		self.current_attribute = None

		self.animation_flag = "enter"
		self.make_player_stats_list()
		self.make_player_inventory_list()
		self.make_player_spells_list()

		self.main_menu = self.components['MainMenu']
		self.display_menu = self.components['DisplayMenu'] 
		self.popup_menu = self.components['Popup']
		self.goldbox = self.components['GoldBox']
		self.levelbox = self.components['LevelBox']
		self.pointsbox = self.components['PointsBox']
		self.surface_box = self.components['SurfaceBox']

	def enter_state(self):
		self.active = self.current 
		self.current_attribute = None
		self.main_menu.clear()
		self.display_menu.clear()
		self.popup_menu.clear()
		self.set_active_menu(self.main_menu, ["Stats", "Inventory", "Spells", "Exit"])
		self.animation_flag = "enter"

	def leave_state(self):
		pass

	def process_event(self, event):
		self.update(event)

	def active_state(self):
		return self.active

	def update(self, event):
		health_bar = Bar(120, 20, setup.player.health(), setup.player.max_health(), red)
		mana_bar = Bar(120, 20, setup.player.mana(), setup.player.max_mana(), blue)
		xp_bar = Bar(120, 20, setup.player.xp(), setup.player.max_xp(), purple)

		self.goldbox.update_message(["Gold: " + str(setup.player.gold())])
		self.levelbox.update_message(["Level: " + str(setup.player.level())])
		self.pointsbox.update_message(["Points: " + str(setup.player.points())])
		self.surface_box.update_surfacebox(["HP","Mana","XP"], [health_bar, mana_bar, xp_bar])
		self.animation_flag = setup.player.animate_stats(self.animation_flag)
		if event != None:
			if event.type == pygame.KEYDOWN:
				self.active_menu.update(event)
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					quit()
				elif event.key == pygame.K_RETURN:
					self.process_key_return(self.active_menu.active())

		if self.animation_flag == "exit":
			self.active = self.previous


	def process_key_return(self, key):
		print key
		if key == "Level Up":
			setup.player.levelup(self.current_attribute)
			self.make_player_stats_list()
			self.set_active_menu(self.display_menu, self.player_stats_list + ["Back"])
		elif key == "Stats":
			self.make_player_stats_list()
			self.set_active_menu(self.display_menu, self.player_stats_list + ["Back"])
		elif key == "Inventory":
			self.make_player_inventory_list()
			self.set_active_menu(self.display_menu, self.player_inventory_list + ["Back"])
		elif key == "Spells":
			self.make_player_spells_list()
			self.set_active_menu(self.display_menu, self.player_spells_list + ["Back"])
		elif key == "Back":
			self.set_active_menu(self.main_menu, ["Stats", "Inventory", "Spells", "Exit"])
		elif key == "Exit":
			self.animation_flag = "leave"
		else:
			if key in self.player_inventory_list:
				setup.player.use(key)
				self.make_player_inventory_list()
				self.set_active_menu(self.display_menu, self.player_inventory_list + ["Back"])

			if key in self.player_stats_list:
				attribute_name = key.split(' ', 1)[0]
				self.current_attribute = attribute_name
				self.set_active_menu(self.popup_menu, ["Level Up", "Back"])
				if setup.player.isLeveled() == False:
					self.active_menu.set_label_unselectable(0)

	def draw(self, display):
		display.blit(self.stats_image, (0,0))
		self.main_menu.draw(display, (150, 600), 300, 400, 20, 40, 20, 50, 100)
		self.display_menu.draw(display, (550, 425), 500, 750, 40, 60, 20, 50, 100)
		if self.active_menu == self.popup_menu:
			self.popup_menu.draw(display, (500,400), 400, 200, 40, 60, 20, 50, 100)
		self.goldbox.draw(display, (700,32), 150, 50)
		self.levelbox.draw(display, (550, 32), 150, 50)
		self.pointsbox.draw(display, (400, 32), 150, 50)
		self.surface_box.draw(display, (150, 325), 300, 125, 100, 20, 20, 10)
		setup.player.draw(display)

	def make_player_stats_list(self):
		self.player_stats_list = []
		self.player_stats_list.append("Strength  " + str(setup.player.strength()))
		self.player_stats_list.append("Agility  " + str(setup.player.agility()))
		self.player_stats_list.append("Constitution  " + str(setup.player.constitution()))
		self.player_stats_list.append("Wisdom  " + str(setup.player.wisdom()))
		self.player_stats_list.append("Intelligence  " + str(setup.player.intelligence()))

	def make_player_inventory_list(self):
		self.player_inventory_list = []
		self.player_inventory_list = setup.player.inventory()

	def make_player_spells_list(self):
		self.player_spells_list = []
		self.player_spells_list = setup.player.spells()

	def popup_message(self, attribute, level):
		pass
