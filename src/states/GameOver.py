import pygame
from constants import *
from tools import *
from State import *


class GameOver(State):
	def __init__(self, gameover_characters, gameover_components, prev, curr, next):
		super(GameOver, self).__init__(gameover_characters, gameover_components, prev, curr, next)
		self.gameover_image = SpriteSheet("../artwork/battle.png").get_image()
		self.gameover_image = pygame.transform.scale(self.gameover_image, (SCREEN_SIZE[0], SCREEN_SIZE[1]))

		self.flag = "enter"
		self.frame = None
		self.alpha = None

	def enter_state(self):
		self.active = 'Game Over'
		self.flag = ""
		self.frame = 1
		self.alpha = 0

	def leave_state(self):
		self.active = self.next
		self.flag = "enter"

	def process_event(self, event):
		self.update(event)

	def active_state(self):
		return self.active

	def update(self, event):
		if self.flag == "enter":
			self.enter_state()

		self.frame += 1

		if event != None:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					quit()
				elif event.key == pygame.K_RETURN:
					self.process_key_return(None)

		if self.flag == "exit":
			self.leave_state()

	def process_key_return(self, key):
		if key == None:
			if self.alpha == 10:
				self.flag = "exit"

	def draw(self, display):
		if self.frame == 30:
			self.alpha = min(self.alpha+1, 10)
			self.frame = 1

		self.image = self.gameover_image.copy()
		self.image.fill((0,0,0,self.alpha), None, pygame.BLEND_RGBA_MULT)
		display.blit(self.image, (0,0))
		message_to_screen("Game Over", white, display, 400, 400)
		if self.alpha == 10:
			message_to_screen("Press Enter", white, display, 400, 600)


class Credits(State):
	def __init__(self, credits_image_filename, prev, curr, next):
		super(Credits, self).__init__(prev, curr, next)
		self.credits_image = SpriteSheet(credits_image_filename).get_image()
		self.credits_image = pygame.transform.scale(self.credits_image, (SCREEN_SIZE[0], SCREEN_SIZE[1]))

		self.credits_dict = None

		self.make_credits_dict()

	def enter_state(self):
		pass

	def leave_state(self):
		pass

	def process_event(self, event):
		self.update(event)

	def active_state(self):
		return self.current

	def update(self, event):
		pass

	def draw(self, display):
		display.blit(self.credits_image, (0, 0))

	def make_credits_dict(self):
		self.credits_dict = {'Programmer': ["James Sandham"],
							 'Artwork': ["Someone"],
							 'Music & Sound': ["Daniel Hoy"]}