import pygame
from constants import *
from tools import *
from Menu import *
from State import *


class Start(State):
	def __init__(self, start_characters, start_components, prev, curr, next):
		super(Start, self).__init__(start_characters, start_components, prev, curr, next)
		self.start_image = SpriteSheet("../artwork/woods.png").get_image()
		self.start_image = pygame.transform.scale(self.start_image, (SCREEN_SIZE[0], SCREEN_SIZE[1]))

		self.menu = self.components['Menu']

	def enter_state(self):
		self.active = self.current
		self.set_active_menu(self.menu, ["New Game", "Load Game", "Settings", "Quit"])

	def leave_state(self):
		pass

	def process_event(self, event):
		self.update(event)

	def active_state(self):
		return self.active

	def update(self, event):
		if event != None:
			if event.type == pygame.KEYDOWN:
				self.menu.update(event)
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					quit()
				elif event.key == pygame.K_RETURN:
					self.process_key_return(self.menu.active())

	def process_key_return(self,key):
		if key == "New Game":
			self.active = self.next
		elif key == "Load Game":
			self.set_active_menu(self.menu, ["Back"])
		elif key == "Settings":
			self.set_active_menu(self.menu, ["Difficulty", "Graphics", "Sound", "Back"])
		elif key == "Quit":
			pygame.quit()
			quit()
		elif key == "Back":
			self.set_active_menu(self.menu, ["New Game", "Load Game", "Settings", "Quit"])

	def draw(self, display):
		display.blit(self.start_image, (0,0))
		self.menu.draw(display, SCREEN_BOTTOM_CENTRE, 250, 300, 20, 20, 20, 50, 100)