import pygame
from Menu import *


class State(object):
	def __init__(self, characters, components, prev, curr, next):
		self.characters = characters
		self.components = components
		self.previous = prev
		self.current = curr
		self.next = next
		self.active = None
		self.active_menu = None
		self.active_component = None


	# "virtual" methods
	def enter_state(self):
		pass

	def leave_state(self):
		pass

	def process_event(self, event):
		pass

	def active_state(self):
		pass

	def update(self, event):
		pass

	def draw(self, display):
		pass

    # "non-virtual" methods
	# def next(self):
	# 	return self.next

	# def current(self):
	# 	return self.current

	# def previous(self):
	# 	return self.previous

	# # change active state
	# def change_active_state(self):
		# pass

	# def set_state(self):
		# pass

	# set active menu
	def set_active_menu(self, menu, menu_list):
		if self.active_menu == None:
			pass
		else:
			self.active_menu.selector_invisible()
		self.active_menu = menu
		self.active_menu.set(menu_list)
		self.active_menu.selector_visible()