import pygame
import setup
import math
from constants import *
from fonts import *
from tools import *
from State import *


class FastTravel(State):
	def __init__(self, fasttravel_characters, fasttravel_components, prev, curr, next):
		super(FastTravel, self).__init__(fasttravel_characters, fasttravel_components, prev, curr, next)
		self.world_image = SpriteSheet("../artwork/overworld.png").get_image()
		self.world_image = pygame.transform.scale(self.world_image, (SCREEN_SIZE[0], SCREEN_SIZE[1]))

		self.frame = 0
		self.partial_path_length = 0
		self.points = []


	def enter_state(self):
		self.active = self.current

	def leave_state(self):
		self.frame = 0
		self.partial_path_length = 0
		self.points = []

	def process_event(self, event):
		self.update(event)

	def active_state(self):
		return self.active

	def update(self, event):
		self.frame = self.frame + 1
		o_x = 800*setup.player.fasttravel_start[0]/(800*32)
		o_y = 800*setup.player.fasttravel_start[1]/(800*32)
		d_x = 800*setup.player.fasttravel_end[0]/(800*32)
		d_y = 800*setup.player.fasttravel_end[1]/(800*32)

		total_path_length = math.sqrt((d_x-o_x)**2 + (d_y-o_y)**2)

		if self.partial_path_length <= total_path_length:
			if self.frame % 40 == 0:
				self.partial_path_length += 10

				slope = float(d_y - o_y) / (d_x - o_x)
				A = math.sqrt((d_x-o_x)**2 + (d_y-o_y)**2) / (d_x-o_x)

				x = (self.partial_path_length + A*o_x) / A
				y = slope*x + o_y - slope*o_x

				self.points.append((x,y))

				# print "slope: " +str(slope)
		else:
			self.active = self.next

		# print "partial path length: " + str(self.partial_path_length)
		# print "total path length: " + str(total_path_length)

		print "origin: " + str(o_x) + ", " + str(o_y)
		print "destination: " + str(d_x) + ", " + str(d_y)

	def draw(self, display):
		# print self.points
	
		display.blit(self.world_image, (0,0))
		for point in self.points:
			pygame.draw.circle(display, (255,0,0,0), (int(point[0]), int(point[1])), 4, 0)
