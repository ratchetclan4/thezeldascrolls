﻿import pygame
import setup
from constants import *
from tools import *
from Equipment import *
from Container import *
from Menu import *
from DialogBox import *
from State import *
from FastTravel import *

class Shop(State):
	def __init__(self, shop_characters, shop_components, prev, curr, next):
		super(Shop, self).__init__(shop_characters, shop_components, prev, curr, next)
		self.shop_image = SpriteSheet("../artwork/battle.png").get_image()
		self.shopkeeper_image = SpriteSheet("../artwork/Pirate.png").image_at((0, 0, 32, 32))
		self.shop_image = pygame.transform.scale(self.shop_image, (SCREEN_SIZE[0], SCREEN_SIZE[1]))
		
		# load artwork generic to all shops from ../artwork
		self.main_menu = self.components['MainMenu']
		self.popup = self.components['Popup']
		self.goldbox = self.components['GoldBox']
		self.message_box = self.components['MessageBox']
		self.dialogue_box = self.components['DialogBox']

		# variables common to all shop types
		self.shop_name = None
		self.shopkeeper_name = None
		self.current_message = None
		self.items_for_sale = None
		self.transaction_type = None
		self.shop_options_list = []
		self.animation_flag = "enter"

	# "virtual methods" implemented by each shop type
	def reset_shop(self):
		pass

	def make_items_for_sale_dict(self):
		pass
		
	# intialize variable upon entering shop
	def enter_state(self):
		self.active = self.current
		self.current_message = self.welcome_message()
		self.reset_shop()
		self.message_box.reset_message()
		self.set_active_menu(self.main_menu, self.shop_options_list)
		self.animation_flag = "enter"

	# reset variables upon leaving shop
	def leave_state(self):
		pass

	def process_event(self, event):
		self.update(event)

	def active_state(self):
		return self.active

	def update(self, event):
		self.goldbox.update_message(["Gold: " + str(setup.player.gold())])
		self.message_box.update_message(self.current_message)
		self.animation_flag = setup.player.animate_shop(self.animation_flag)
		if event != None:
			if event.type == pygame.KEYDOWN:
				self.main_menu.update(event)
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					quit()
				elif event.key == pygame.K_RETURN:
					self.process_key_return(self.main_menu.active())

		if self.animation_flag == "exit shop":
			self.active = self.previous

	def process_key_return(self, key):
		if key == "Buy":
			self.transaction_type = 'buy'
			self.current_message = self.buy_message()
			self.message_box.reset_message()
			self.set_active_menu(self.main_menu, self.items_for_sale.keys() + ["Back"])
		elif key == "Sell":
			self.transaction_type = 'sell'
			self.current_message = self.sell_message()
			self.message_box.reset_message()
			self.set_active_menu(self.main_menu, setup.player.inventory() + setup.player.spells() + ["Back"])
		elif key == "Save":
			self.current_message = self.save_message()
			self.message_box.reset_message()
			self.set_active_menu(self.main_menu, ["Back"])
		elif key == "Travel":
			self.current_message = self.travel_message()
			self.message_box.reset_message()
			self.set_active_menu(self.main_menu, setup.player.cities() + ["Back"])
			# fasttravel_components = {}
			# self.states['FastTravel'] = FastTravel({}, stats_components, {}, self.next, 'FastTravel', None)
			# self.active = 'FastTravel'
		elif key == "Enchant":
			pass
		elif key == "Repair":
			pass
		elif key == "Craft":
			pass
		elif key == "Leave":
			self.current_message = self.leave_message()
			self.message_box.reset_message()
			self.main_menu.selector_invisible()
			self.animation_flag = "leave"
		elif key == "Back":
			self.transaction_type = None
			self.current_message = self.back_message()
			self.message_box.reset_message()
			self.set_active_menu(self.main_menu, self.shop_options_list)
		elif key == None:
			pass
		else: 
			if self.transaction_type == 'buy':
				label = key
				item = self.items_for_sale.get_item(label)
				setup.player.buy(label, item)
				self.current_message = self.purchase_message(label, 1)
				self.message_box.reset_message()
			elif self.transaction_type == 'sell':
				label = key
				item = setup.player.sell(label)
				self.items_for_sale.add(label, item)
				self.current_message = self.sale_message(label, 1)
				self.set_active_menu(self.main_menu, setup.player.inventory() + setup.player.spells() + ["Back"])
				self.message_box.reset_message()

			if key in setup.player.cities():
				setup.player.fasttravel(self.previous, key)   ######################################################
				print "travel start location: " + str(self.previous)
				print "travel destination: " + str(key)
				self.active = 'FastTravel'

	def welcome_message(self):
		return ["Select an option"]

	def buy_message(self):
		return ["Choose an item to buy..."]

	def sell_message(self):
		return ["Choose an item to sell..."]

	def save_message(self):
		return ["Feature not yet implemented"]

	def leave_message(self):
		return ["Leaving shop!"]

	def purchase_message(self, item, gold):
		return ["You have purchased one " + str(item), 
		        "Gold deceased by " + str(gold),
		        "Choose an item to buy..."]

	def insufficient_gold_message(self):
		return ["You do not have enough gold for that purchase"]

	def sale_message(self, item, gold):
		return ["You have sold one " + str(item),
		        "Gold increased by " + str(gold),
		        "Choose an item to sell..."]

	def back_message(self):
		return ["Select an option"]

	def travel_message(self):
		return ["Select city to travel to"]




class TavernShop(Shop):
	def __init__(self, shop_characters, shop_components, prev, curr, next):
		super(TavernShop, self).__init__(shop_characters, shop_components, prev, curr, next)
		self.shop_name = 'The Prancing Pony'
		self.shopkeeper_name = 'Professor Kevstone'

		self.make_items_for_sale_dict()

		self.shopkeeper_image = pygame.transform.scale(self.shopkeeper_image, (128,128))
		self.barrel_image = SpriteSheet("../artwork/barrel.png").image_at((0,0,128,128))
		self.table_image = SpriteSheet("../artwork/table.png").image_at((0,0,30,87))
		self.table_image = pygame.transform.scale(self.table_image, (100,275))

	def reset_shop(self):
		self.active = self.current
		self.shop_options_list = ["Buy", "Sell", "Save", "Leave"]
		self.set_active_menu(self.main_menu, self.shop_options_list)

	def make_items_for_sale_dict(self):
		self.items_for_sale = Container()
		self.items_for_sale.add('Minor Health', MinorHealth("../artwork/health_potion.png"))
		self.items_for_sale.add('Major Health', MajorHealth("../artwork/health_potion.png"))

	def draw(self, display):
		display.blit(self.shop_image, (0,0))
		display.blit(self.shopkeeper_image, (600, 200))
		display.blit(self.barrel_image, (640, 100))
		display.blit(self.barrel_image, (680, 140))
		display.blit(self.barrel_image, (500, 90))
		display.blit(self.barrel_image, (150,100))
		display.blit(self.barrel_image, (100, 500))
		display.blit(self.table_image, (500, 200))

		self.main_menu.draw(display, SCREEN_BOTTOM_RIGHT, 250, 300, 20, 20, 20, 50, 100)
		self.goldbox.draw(display, (100,50), 150, 50)
		self.message_box.draw(display, (250,650), 500, 300)
		setup.player.draw(display)


		


class ArmourShop(Shop):
	def __init__(self, shop_characters, shop_components, prev, curr, next):
		super(ArmourShop, self).__init__(shop_characters, shop_components, prev, curr, next)
		self.shop_name = 'The Iron and Hammer'
		self.shopkeeper_name = 'Brood Stronghammer'

		self.make_items_for_sale_dict()

		self.shopkeeper_image = pygame.transform.scale(self.shopkeeper_image, (128,128))
		self.armour_shelf_image = SpriteSheet("../artwork/armour_shelf.png").image_at((0,0,66,58))
		self.armour_shelf_image = pygame.transform.scale(self.armour_shelf_image, (250, 200))
		self.table_image = SpriteSheet("../artwork/table.png").image_at((0,0,30,87))
		self.table_image = pygame.transform.scale(self.table_image, (100,275))

	def reset_shop(self):
		self.active = self.current
		self.shop_options_list = ["Buy", "Sell", "Repair", "Leave"]
		self.set_active_menu(self.main_menu, self.shop_options_list)

	def make_items_for_sale_dict(self):
		self.items_for_sale = Container()
		self.items_for_sale.add('Leather', Leather("../artwork/armour.png"))

	def draw(self, display):
		display.blit(self.shop_image, (0,0))
		display.blit(self.shopkeeper_image, (600, 200))
		display.blit(self.armour_shelf_image, (50, 50))
		display.blit(self.armour_shelf_image, (300, 50))
		display.blit(self.table_image, (500, 200))

		self.main_menu.draw(display, SCREEN_BOTTOM_RIGHT, 250, 300, 20, 20, 20, 50, 100)
		self.goldbox.draw(display, (100,50), 150, 50)
		self.message_box.draw(display, (250,650), 500, 300)
		setup.player.draw(display)





class WeaponShop(Shop):
	def __init__(self, shop_characters, shop_components, prev, curr, next):
		super(WeaponShop, self).__init__(shop_characters, shop_components, prev, curr, next)
		self.shop_name = 'Fine Weapons'
		self.shopkeeper_name = 'Kevin'

		self.make_items_for_sale_dict()

		self.shopkeeper_image = pygame.transform.scale(self.shopkeeper_image, (128,128))
		self.hanging_sword_image = SpriteSheet("../artwork/hanging_sword.png").image_at((0,0,34,25))
		self.hanging_sword_image = pygame.transform.scale(self.hanging_sword_image, (150, 100))
		self.hanging_shield_image = SpriteSheet("../artwork/hanging_shield.png").image_at((0,0,30,31))
		self.hanging_shield_image = pygame.transform.scale(self.hanging_shield_image, (100, 100))
		self.hanging_axe_image = SpriteSheet("../artwork/hanging_axe.png").image_at((0,0,35,17))
		self.hanging_axe_image = pygame.transform.scale(self.hanging_axe_image, (150, 50))
		self.table_image = SpriteSheet("../artwork/table.png").image_at((0,0,30,87))
		self.table_image = pygame.transform.scale(self.table_image, (100,275))

	def reset_shop(self):
		self.active = self.current
		self.shop_options_list = ["Buy", "Sell", "Craft", "Leave"]
		self.set_active_menu(self.main_menu, self.shop_options_list)

	def make_items_for_sale_dict(self):
		self.items_for_sale = Container()
		self.items_for_sale.add('Iron Dagger', IronDagger("../artwork/sword.png"))
		self.items_for_sale.add('Iron Sword', IronSword("../artwork/sword.png"))

	def draw(self, display):
		display.blit(self.shop_image, (0,0))
		display.blit(self.shopkeeper_image, (600, 200))
		display.blit(self.hanging_sword_image, (50, 100))
		display.blit(self.hanging_sword_image, (350, 100))
		display.blit(self.hanging_shield_image, (225, 100))
		display.blit(self.hanging_axe_image, (640, 100))
		display.blit(self.table_image, (500, 200))

		self.main_menu.draw(display, SCREEN_BOTTOM_RIGHT, 250, 300, 20, 20, 20, 50, 100)
		self.goldbox.draw(display, (100,50), 150, 50)
		self.message_box.draw(display, (250,650), 500, 300)
		setup.player.draw(display)




class PotionShop(Shop):
	def __init__(self, shop_characters, shop_components, prev, curr, next):
		super(PotionShop, self).__init__(shop_characters, shop_components, prev, curr, next)
		self.shop_name = 'Bartlebee\'s Elixirs, Spells, and Potions'
		self.shopkeeper_name = 'Bartlebee'

		self.make_items_for_sale_dict()

		self.shopkeeper_image = pygame.transform.scale(self.shopkeeper_image, (128,128))
		self.barrel_image = SpriteSheet("../artwork/barrel.png").image_at((0,0,128,128))
		self.potion_shelf_image = SpriteSheet("../artwork/potion_shelf.png").image_at((0,0,250,385))
		self.potion_shelf_image = pygame.transform.scale(self.potion_shelf_image, (150,250))
		self.table_image = SpriteSheet("../artwork/table.png").image_at((0,0,30,87))
		self.table_image = pygame.transform.scale(self.table_image, (100,275))
		self.health_potion_image = SpriteSheet("../artwork/health_potion.png").image_at((0,0,32,32))
		self.health_potion_image = pygame.transform.scale(self.health_potion_image, (64,64))
		self.mana_potion_image = SpriteSheet("../artwork/mana_potion.png").image_at((0,0,32,32))
		self.mana_potion_image = pygame.transform.scale(self.mana_potion_image, (64,64))
		self.spell_scroll_image = SpriteSheet("../artwork/spell_scroll.png").image_at((0,0,32,32))
		self.spell_scroll_image = pygame.transform.scale(self.spell_scroll_image, (64,64))

	def reset_shop(self):
		self.active = self.current
		self.shop_options_list = ["Buy", "Sell", "Enchant", "Leave"]
		self.set_active_menu(self.main_menu, self.shop_options_list)

	def make_items_for_sale_dict(self):
		self.items_for_sale = Container()
		self.items_for_sale.add('Minor Health', MinorHealth("../artwork/health_potion.png"))
		self.items_for_sale.add('Major Health', MajorHealth("../artwork/health_potion.png"))
		self.items_for_sale.add('Scroll of Fireball', ScrollOfFireball("../artwork/spell_scroll.png"))
		self.items_for_sale.add('Scroll of Blizzard', ScrollOfBlizzard("../artwork/spell_scroll.png"))
		self.items_for_sale.add('Scroll of Whirlwind', ScrollOfWhirlwind("../artwork/spell_scroll.png"))

	def draw(self, display):
		display.blit(self.shop_image, (0,0))
		display.blit(self.shopkeeper_image, (600, 200))
		display.blit(self.barrel_image, (640, 100))
		display.blit(self.potion_shelf_image, (50, 50))
		display.blit(self.potion_shelf_image, (300, 50))
		display.blit(self.table_image, (500, 200))
		display.blit(self.health_potion_image, (510, 200))
		display.blit(self.mana_potion_image, (530, 210))
		display.blit(self.spell_scroll_image, (520, 370))

		self.main_menu.draw(display, SCREEN_BOTTOM_RIGHT, 250, 300, 20, 20, 20, 50, 100)
		self.goldbox.draw(display, (100,50), 150, 50)
		self.message_box.draw(display, (250,650), 500, 300)
		setup.player.draw(display)



class Wharf(Shop):
	def __init__(self, shop_characters, shop_components, prev, curr, next):
		super(Wharf, self).__init__(shop_characters, shop_components, prev, curr, next)
		self.shop_name = 'The Flotsam and Jetsam'
		self.shopkeeper_name = 'Captain Seaborn'

		self.make_items_for_sale_dict()

		self.shopkeeper_image = pygame.transform.scale(self.shopkeeper_image, (128,128))
		self.barrel_image = SpriteSheet("../artwork/barrel.png").image_at((0,0,128,128))
		self.table_image = SpriteSheet("../artwork/table.png").image_at((0,0,30,87))
		self.table_image = pygame.transform.scale(self.table_image, (100,275))

		# setup.player.add_city(prev, (0,0))  ##########################################################

	def reset_shop(self):
		self.active = self.current
		self.shop_options_list = ["Buy", "Sell", "Travel", "Leave"]
		self.set_active_menu(self.main_menu, self.shop_options_list)

	def make_items_for_sale_dict(self):
		self.items_for_sale = Container()

	def draw(self, display):
		display.blit(self.shop_image, (0,0))
		display.blit(self.shopkeeper_image, (600, 200))
		display.blit(self.barrel_image, (640, 100))
		display.blit(self.barrel_image, (680, 140))
		display.blit(self.barrel_image, (500, 90))
		display.blit(self.barrel_image, (150,100))
		display.blit(self.barrel_image, (100, 500))
		display.blit(self.table_image, (500, 200))

		self.main_menu.draw(display, SCREEN_BOTTOM_RIGHT, 250, 300, 20, 20, 20, 50, 100)
		self.goldbox.draw(display, (100,50), 150, 50)
		self.message_box.draw(display, (250,650), 500, 300)
		setup.player.draw(display)