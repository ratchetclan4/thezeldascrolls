import pygame
from Component import *


class StatScreen(Component):
	"""docstring for StatSreen"""
	def __init__(self, character, background_image_filename, selector_image_filename, menu_image_filename):
		super(StatScreen, self).__init__(self, )
		self.arg = arg




	def update(self, event):
		self.health_bar.update(setup.player.health_fraction())
		self.mana_bar.update(setup.player.mana_fraction())
		self.xp_bar.update(setup.player.xp_fraction())
		self.goldbox.update_message(["Gold: " + str(setup.player.gold())])
		self.levelbox.update_message(["Level: " + str(setup.player.level())])
		self.pointsbox.update_message(["Points: " + str(setup.player.points())])
		self.surface_box.update_surfacebox(["HP","Mana","XP"], [self.health_bar, self.mana_bar, self.xp_bar])
		self.flag = setup.player.animate_stats(self.flag)
		if event != None:
			if event.type == pygame.KEYDOWN:
				self.active_menu.update(event)
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					quit()
				elif event.key == pygame.K_RETURN:
					self.process_key_return(self.active_menu.active())

	def process_key_return(self, key):
		if key == "Level Up":
			setup.player.levelup(self.current_attribute)
			self.make_player_stats_list()
			self.set_active_menu(self.display_menu, self.player_stats_list + ["Back"])
		elif key == "Stats":
			self.make_player_stats_list()
			self.set_active_menu(self.display_menu, self.player_stats_list + ["Back"])
		elif key == "Inventory":
			self.make_player_inventory_list()
			self.set_active_menu(self.display_menu, self.player_inventory_list + ["Back"])
		elif key == "Spells":
			self.make_player_spells_list()
			self.set_active_menu(self.display_menu, self.player_spells_list + ["Back"])
		elif key == "Back":
			self.set_active_menu(self.main_menu, ["Stats", "Inventory", "Spells", "Exit"])
		elif key == "Exit":
			self.flag = "leave"
		else:
			if key in self.player_inventory_list:
				setup.player.use(key)
				self.make_player_inventory_list()
				self.set_active_menu(self.display_menu, self.player_inventory_list + ["Back"])

			if key in self.player_stats_list:
				attribute_name = key.split(' ', 1)[0]
				self.current_attribute = attribute_name
				self.set_active_menu(self.popup_menu, ["Level Up", "Back"])
				if setup.player.isLeveled() == False:
					self.active_menu.set_label_unselectable(0)

	def draw(self, display):
		display.blit(self.stats_image, (0,0))
		self.main_menu.draw(display, (150, 600), 300, 400, 20, 40, 20, 50, 100)
		self.display_menu.draw(display, (550, 425), 500, 750, 40, 60, 20, 50, 100)
		if self.active_menu == self.popup_menu:
			self.popup_menu.draw(display, (500,400), 400, 200, 40, 60, 20, 50, 100)
		self.goldbox.draw(display, (700,32), 150, 50)
		self.levelbox.draw(display, (550, 32), 150, 50)
		self.pointsbox.draw(display, (400, 32), 150, 50)
		self.surface_box.draw(display, (150, 325), 300, 125, 100, 20, 20, 10)
		setup.player.draw(display)

	def make_player_stats_list(self):
		self.player_stats_list = []
		self.player_stats_list.append("Strength  " + str(setup.player.strength()))
		self.player_stats_list.append("Agility  " + str(setup.player.agility()))
		self.player_stats_list.append("Constitution  " + str(setup.player.constitution()))
		self.player_stats_list.append("Wisdom  " + str(setup.player.wisdom()))
		self.player_stats_list.append("Intelligence  " + str(setup.player.intelligence()))

	def make_player_inventory_list(self):
		self.player_inventory_list = []
		self.player_inventory_list = setup.player.inventory()

	def make_player_spells_list(self):
		self.player_spells_list = []
		self.player_spells_list = setup.player.spells()


