import pygame
import xml.etree.ElementTree as ET
from tools import *
from constants import *
from Component import *


# -------------------------------------------------------------------|     _
# |      |                                                           |     |
# |      dy                                                          |     |
# |      |                                                           |     |
# |--dx--*---------------------------------------------------|       |     |
# |      |                                                   |       |     |
# |      |                                                   |       |     |
# |      |    lines of dialog goes in here                   |       |     |
# |      |                                                   |       |     |
# |      |                                                   |       |     |
# |      |                                                   |       |     |
# |      |                                                   |       |     |
# |      |---------------------------------------------------|       |     |
# |                                                                  |   height
# |      |----------| |--------------------------------------|       |     |
# |      |          | |                                      |       |     |
# |      | selector | |  player response goes here           |       |     |
# |      |          | |                                      |       |     |
# |      |----------| |--------------------------------------|       |     |
# |                                                                  |     |
# |      |----------| |--------------------------------------|       |     |
# |      |          | |                                      |       |     |
# |      | selector | |  player response goes here           |       |     |
# |      |          | |                                      |       |     |
# |      |----------| |--------------------------------------|       |     |
# |                                                                  |     |
# |      |-selector-|---------dialog_width-------------------|       |     |
# |          width                                                   |     |
# |------------------------------------------------------------------|     -
#    
# |---------------------------width----------------------------------|
class DialogBox(Component):
	def __init__(self, selector_image_filename, box_image_filename):
		super(DialogBox, self).__init__()
		self.selector_visible_flag = True
		self.selector_image = pygame.image.load(selector_image_filename)
		self.box_image = pygame.image.load(box_image_filename)
		
		self.flag = False
		self.xml_tree = None
		self.dialogue_id = 0
		self.response_id = 0
		self.dialogue = None
		self.responses = None
		self.numResponses = None

	def attach(self, dialogue_xml_tree):
		self.xml_tree = dialogue_xml_tree
		self.active = True
		self.visible = True
		self.set_dialogue()
		self.set_responses()

	def detach(self):
		self.reset()
		self.active = False
		self.visible = False

	def reset(self):
		self.flag = False
		self.xml_tree = None
		self.dialogue_id = 0
		self.response_id = 0
		self.dialogue = None 
		self.responses = None
		self.numResponses = None

	def update(self, event):
		if event != None:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_RETURN:
					if self.flag == False:
						self.flag = True
					else:
						self.next()
				elif event.key == pygame.K_UP:
					self.up()
				elif event.key == pygame.K_DOWN:
					self.down()

	def next(self):
		if self.xml_tree[self.dialogue_id][0].attrib['type'] == "end":
			self.detach()
		else:
			self.dialogue_id = int(self.xml_tree[self.dialogue_id][self.response_id + 1].attrib['nextDialogue'])
			self.response_id = 0
			self.set_dialogue()
			self.set_responses()

	def up(self):
		self.response_id = min(0, self.numResponses)

	def down(self):
		self.response_id = min(1, self.numResponses)

	def draw(self, display_window, location, width, height, dialog_width, selector_width, dx, dy):
		if self.visible == True:
			# find top left corner to blit image based on location
			x = location[0] - width/2
			y = location[1] - height/2

			# scale box image to width and height
			self.box_image = pygame.transform.scale(self.box_image, (width, height))

			# blit box image to screen
			display_window.blit(self.box_image, (x,y))

			# print dialogue line to screen
			dialogue_surface = TextSurface(self.dialogue, "freesans", 20, black, dialog_width)
			display_window.blit(dialogue_surface, (x+dx, y+dy))

			# blit selector and responses
			self.selector_image = pygame.transform.scale(self.selector_image, (selector_width, selector_width))
			for i in range(0,self.numResponses):
				response_surface = TextSurface(self.responses[i], "freesans", 20, black, dialog_width)
				display_window.blit(response_surface, (x+dx+selector_width, y+2*dy+response_surface.get_height()+i*selector_width))

			display_window.blit(self.selector_image, (x + dx, y + 2*dy + response_surface.get_height() + self.response_id*selector_width))

	def set_dialogue(self):
		self.dialogue = self.xml_tree[self.dialogue_id][0].text.lstrip().rstrip()

	def set_responses(self):
		self.numResponses = int(self.xml_tree[self.dialogue_id][0].attrib['numResponses'])
		self.responses = []
		for i in range(0,self.numResponses):
			response_entry = self.xml_tree[self.dialogue_id][i + 1].text.lstrip().rstrip()
			self.responses.append(response_entry)





# -----------------------------------------------------------------|     _
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     | 
# |                                                                |     |
# |                     message goes here                          |     |
# |                                                                |   height
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |----------------------------------------------------------------|     -
#    
# |-------------------------- width--------------------------------|
class MessageBox(Component):
	def __init__(self, box_image_filename):
		self.box_image = pygame.image.load(box_image_filename)
		self.active_dialogue = None
		self.delay = 0
		self.line = 0

	def update_message(self, string_list):
		self.active_dialogue = string_list[self.line]
		self.delay += 1
		if self.delay > 80:
			self.line += 1
			self.delay = 0

		self.line = min(self.line, len(string_list)-1)


	def reset_message(self):
		self.delay = 0
		self.line = 0

	def draw(self, display_window, location, width, height):
		# find top left corner to blit image based on location
		x = location[0] - width/2
		y = location[1] - height/2

		# scale image to width and height
		self.box_image = pygame.transform.scale(self.box_image, (width, height))

		# blit menu image to screen
		display_window.blit(self.box_image, (x,y))

		# print line to screen
		message_to_screen(self.active_dialogue, black, display_window, location[0], location[1])




# -----------------------------------------------------------------|     _
# |      |                                                         |     |
# |      dy                                                        |     |
# |      |                                                         |     |
# |--dx--|--------||------------------------------------------|    |     |
# |      |        ||                                          |    |     |
# |      |  label ||       surface 1                          |    |     |
# |      |        ||                                          |    |     |
# |      |--------||------------------------------------------|    |     |
# |                           |                                    |     | 
# |                           dz                                   |     |
# |                           |                                    |   height
# |      |--------||------------------------------------------|    |     |
# |      |        ||                                          |    |     |
# |      |  label ||       surface 2                          |    |     |
# |      |        ||                                          |    |     |
# |      |--------||------------------------------------------|    |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |----------------------------------------------------------------|     -
#    
# |-------------------------- width--------------------------------|
class SurfaceBox(Component):
	def __init__(self, box_image_filename):
		self.box_image = pygame.image.load(box_image_filename)
		self.label_list = None
		self.surface_list = None

	def update_surfacebox(self, label_list, surface_list):
		self.label_list = label_list
		self.surface_list = surface_list

	def draw(self, display_window, location, width, height, label_width, dx, dy, dz):
		# find top left corner to blit image based on location
		x = location[0] - width/2
		y = location[1] - height/2

		# scale image to width and height
		self.box_image = pygame.transform.scale(self.box_image, (width, height))

		# blit menu image to screen
		display_window.blit(self.box_image, (x,y))

		# blit label_list and surface_list to screen
		offset = 0
		index = 0
		for surf in self.surface_list:
			label = self.label_list[index]
			message_to_screen(label, black, display_window, x + dx + label_width/2, y + dy + offset)
			display_window.blit(surf, (x + dx + label_width, y + dy + offset))
			offset += surf.get_height() + dz
			index += 1
