import pygame


class Component(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.active = False  # sets whether component will be updated by events
		self.visible = False # sets whether component will be drawn

	# virtual methods
	def update(self, event):
		pass

	def draw(self, display, shift_x = 0, shift_y = 0):
		pass

	# # "non-virtual methods"
	# def set_active(self):
	# 	self.active = True

	# def set_inactive(self):
	# 	self.active = False

	# def set_visible(self):
	# 	self.visible = True

	# def set_invisible(self):
	# 	self.visible = False

	# def switch(self):
	# 	if self.active == True:
	# 		self.active = False
	# 	else:
	# 		self.active = True

