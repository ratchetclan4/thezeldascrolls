import pygame
from Component import *
from Menu import *


class StartSreen(Component):
	"""docstring for StartSreen"""
	def __init__(self, background_image_filename, selector_image_filename, menu_image_filename):
		super(StartSreen, self).__init__(self)
		self.background_image = pygame.image.load(background_image_filename) 
		self.background_image = pygame.transform.scale(self.background_image, SCREEN_SIZE)
		self.menu = Menu(selector_image_filename, menu_image_filename)

	def update(self, event):
		if event != None:
			if event.type == pygame.KEYDOWN:
				self.menu.update(event)
				if event.key == pygame.K_RETURN:
					self.process_key_return(self.menu.active())


	def process_key_return(self,key):
		if key == "New Game":
			self.active = False
			self.visible = False
		elif key == "Load Game":
			self.menu.set(["Back"])
		elif key == "Settings":
			self.menu(["Difficulty", "Graphics", "Sound", "Back"])
		elif key == "Quit":
			pygame.quit()
			quit()
		elif key == "Back":
			self.menu(["New Game", "Load Game", "Settings", "Quit"])

	def draw(self, display):
		display.blit(self.start_image, (0,0))
		self.menu.draw(display, SCREEN_BOTTOM_CENTRE, 250, 300, 20, 20, 20, 50, 100)