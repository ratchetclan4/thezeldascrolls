# -----------------------------------------------------------------|     _
# |      |                                                         |     |
# |      dy                                                        |     |
# |      |                                                         |     |
# |--dx--*--------||------------------------------------------|    |     |
# |      |        ||                                          |    |     |
# |      |        ||  text label/surface label goes here      |    |     |
# |      |        ||                                          |    |     |
# |      |--------||------------------------------------------|    |     | 
# |                                |                               |     |
# |                                dz                              |     |
# |                                |                               |   height
# |      |--------||------------------------------------------|    |     |
# |      |        ||                                          |    |     |
# |      |        ||  text label/surface label goes here      |    |     |
# |      |        ||                                          |    |     |
# |      |--------||------------------------------------------|    |     |
# |                                                                |     |
# |      selector_width            label_width                     |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |----------------------------------------------------------------|     -
#    
# |-------------------------- width--------------------------------|


import pygame
from colors import *
from fonts import *
from tools import *
from Component import *


class Menu(Component):
	def __init__(self, selector_image_filename, menu_image_filename, max_labels = 4):
		self.selector_visible_flag = True
		self.selector_image = pygame.image.load(selector_image_filename)
		self.menu_image = pygame.image.load(menu_image_filename)

		self.max_labels = max_labels
		self.number_of_labels = 0
		self.number_of_visible_labels = 0
		self.active_label = 0
		self.visible_index = 0

		self.labels = []
		self.visible_labels = []

	def selector_visible(self):
		self.selector_visible_flag = True

	def selector_invisible(self):
		self.selector_visible_flag = False

	def add(self, label_list):
		self.active_label = 1
		self.visible_index = 1
		self.number_of_labels += len(label_list)
		self.number_of_visible_labels = min(self.max_labels, self.number_of_labels)
		self.labels += label_list

		self.visible_labels = []
		for i in range(0, self.number_of_visible_labels):
			self.visible_labels.append(self.labels[i])

	def set(self, label_list):
		self.active_label = 1
		self.visible_index = 1
		self.number_of_labels = len(label_list)
		self.number_of_visible_labels = min(self.max_labels, self.number_of_labels)
		self.labels = label_list

		self.visible_labels = []
		for i in range(0, self.number_of_visible_labels):
			self.visible_labels.append(self.labels[i])


	def clear(self):
		self.selector_visible_flag = False
		self.active_label = 0
		self.visible_index = 0
		self.number_of_labels = 0
		self.number_of_visible_labels = 0
		self.labels = []
		self.visible_labels = []

	def active(self):
		if self.selector_visible_flag == True:
			return self.labels[self.active_label-1]
		else:
			return None

	def active_index(self):
		return self.active_label-1

	def size(self):
		return self.number_of_labels

	def update(self, event):
		if self.selector_visible_flag == True:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_UP:
					self.up()
				elif event.key == pygame.K_DOWN:
					self.down()


	def up(self):
		self.active_label = max(self.active_label - 1, 1)
		if self.active_label < self.visible_index:
			self.visible_index -= 1

		for i in range(1, self.number_of_visible_labels + 1):
			self.visible_labels[i-1] = self.labels[self.visible_index - 1 + i - 1]


	def down(self):
		self.active_label = min(self.active_label + 1, self.number_of_labels)
		if self.active_label >= self.visible_index + self.max_labels:
			self.visible_index += 1

		for i in range(1, self.number_of_visible_labels + 1):
			self.visible_labels[i-1] = self.labels[self.visible_index -1 + i - 1]


	def draw(self, display_window, location, width, height, dx, dy, dz, selector_width, label_width):
		# find top left corner to blit image based on location
		x = location[0] - width/2
		y = location[1] - height/2

		# blit menu image to screen
		self.menu_image = pygame.transform.scale(self.menu_image, (width, height))
		display_window.blit(self.menu_image, (x,y))

		# blit menu selector
		if self.selector_visible_flag == True: 
			self.selector_image = pygame.transform.scale(self.selector_image, (selector_width, selector_width))
			temp = self.active_label - self.visible_index
			display_window.blit(self.selector_image, (x + dx, y + dy + temp*(selector_width+dz)))

		# blit visible labels
		for i in range(1, self.number_of_visible_labels+1):
			item_location_x = x + dx + selector_width + label_width/2
			item_location_y = y + dy + selector_width/2 + (i-1)*(selector_width + dz)

			# draw labels depending on whether they are text strings or pygame surfaces
			if isinstance(self.visible_labels[i-1], basestring):
				message_to_screen(self.visible_labels[i-1], black, display_window, item_location_x, item_location_y)
			elif isinstance(self.visible_labels[i-1], pygame.Surface):
				display_window.blit(self.visible_labels[i-1], (item_location_x, item_location_y))





class SpriteMenu(Component):
	def __init__(self, selector_image_filename):
		self.selector_visible_flag = True
		self.selector_image = pygame.image.load(selector_image_filename)
	
		self.number_of_sprites = 0
		self.active_sprite = 0

		self.sprites = []

	def selector_visible(self):
		self.selector_visible_flag = True

	def selector_invisible(self):
		self.selector_visible_flag = False

	def add(self, sprite_list):
		self.active_sprite = 1
		self.number_of_sprites += len(sprite_list)
		self.sprites += sprite_list

	def set(self, sprite_list):
		self.active_sprite = 1
		self.number_of_sprites = len(sprite_list)
		self.sprites = sprite_list

	def remove(self, sprite):
		index = 0
		for spt in self.sprites:
			if spt == sprite:
				self.number_of_sprites -= 1
				if index < self.active_sprite - 1:
					self.active_sprite -= 1	
				del self.sprites[index]
				break
			index += 1

	def clear(self):
		self.selector_visible_flag = False
		self.active_sprite = 0
		self.number_of_sprites = 0
		self.sprites = []

	def active(self):
		if self.selector_visible_flag == True:
			return self.sprites[self.active_sprite-1]
		else:
			return None

	def active_index(self):
		return self.active_sprite-1

	def size(self):
		return self.number_of_sprites

	def update(self, event):
		if self.selector_visible_flag == True:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_UP:
					self.up()
				elif event.key == pygame.K_DOWN:
					self.down()


	def up(self):
		self.active_sprite = max(self.active_sprite - 1, 1)

	def down(self):
		self.active_sprite = min(self.active_sprite + 1, self.number_of_sprites)

	def draw(self, display_window, selector_width):
		if self.number_of_sprites >= 1:
			# find location of menu selector
			x = self.sprites[self.active_sprite - 1].rect.x - selector_width
			y = self.sprites[self.active_sprite - 1].rect.y

			# draw selector
			if self.selector_visible_flag == True: 
				self.selector_image = pygame.transform.scale(self.selector_image, (selector_width, selector_width))
				display_window.blit(self.selector_image, (x, y))

		# draw sprites
		for sprite in self.sprites:
			sprite.image = pygame.transform.scale(sprite.image, (128, 128))
			display_window.blit(sprite.image, (sprite.rect.x, sprite.rect.y))








# -----------------------------------------------------------------|     _
# |      |                                                         |     |
# |      dy                                                        |     |
# |      |                                                         |     |
# |--dx--*----------------------------------------------------|    |     |
# |      |                                                    |    |     |
# |      |                                                    |    |     |
# |      |                                                    |    |     |
# |      |         Text information goes here...              |    |     | 
# |      |                                                    |    |     |
# |      |                                                    |    |     |
# |      |                                                    |    |   height
# |      |                                                    |    |     |
# |      |----------------------------------------------------|    |     |
# |                                |                               |     |
# |                                dz                              |     |
# |                                |     |selector||--text width-| |     |
# |                                        width                   |     |
# |       |-------||--------------|      |-------||--------------| |     |
# |       |       ||              |      |       ||              | |     |
# |       |       ||  text label  |      |       ||  text label  | |     |
# |       |       ||              |      |       ||              | |     |
# |       |-------||--------------|      |-------||--------------| |     |
# |                                                                |     |
# |----------------------------------------------------------------|     -
#    
# |-------------------------- width--------------------------------|
class BinaryInfoMenu(Component):
	def __init__(self, selector_image_filename, menu_image_filename):
		self.selector_visible_flag = True
		self.selector_image = pygame.image.load(selector_image_filename)
		self.menu_image = pygame.image.load(menu_image_filename)

		self.number_of_labels = 0
		self.active_label = 0

		self.labels = []
		self.selectable_labels = []
		self.text_info = None

	def selector_visible(self):
		self.selector_visible_flag = True

	def selector_invisible(self):
		self.selector_visible_flag = False

	def add(self, label_list):
		if len(label_list) != 2:
			print "ERROR: BinaryInfoMenu takes exactly two text labels"
		else:
			self.active_label = 1
			self.number_of_labels = len(label_list)
			self.labels = label_list
			self.selectable_labels = 2 * [True]

	def set(self, label_list):
		if len(label_list) != 2:
			print "ERROR: BinaryInfoMenu takes exactly two text labels"
		else:
			self.active_label = 1
			self.number_of_labels = len(label_list)
			self.labels = label_list
			self.selectable_labels = 2 * [True]

	def clear(self):
		self.selector_visible_flag = False
		self.active_label = 0
		self.number_of_labels = 0
		self.labels = []
		self.selectable_labels = []
		self.text_info = None

	def set_label_unselectable(self, index):
		if index < self.number_of_labels:
			self.selectable_labels[index] = False 


	def active(self):
		if self.selector_visible_flag == True:
			if self.selectable_labels[self.active_label-1] == True:
				return self.labels[self.active_label-1]
			else:
				return None
		else:
			return None

	def active_index(self):
		return self.active_label-1

	def size(self):
		return self.number_of_labels

	def update(self, event):
		if self.selector_visible_flag == True:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT:
					self.left()
				elif event.key == pygame.K_RIGHT:
					self.right()


	def left(self):
		self.active_label = max(self.active_label - 1, 1)

	def right(self):
		self.active_label = min(self.active_label + 1, self.number_of_labels)

	def draw(self, display_window, location, width, height, dx, dy, dz, selector_width, label_width):
		# find top left corner to blit image based on location
		x = location[0] - width/2
		y = location[1] - height/2

		# blit menu image to screen
		self.menu_image = pygame.transform.scale(self.menu_image, (width, height))
		display_window.blit(self.menu_image, (x,y))

		# blit menu selector
		if self.selector_visible_flag == True: 
			self.selector_image = pygame.transform.scale(self.selector_image, (selector_width, selector_width))
			temp = self.active_label - 1
			display_window.blit(self.selector_image, (x + dx + temp*(selector_width + label_width + dz), y + dy + dz))


		# blit labels
		for i in range(1, self.number_of_labels+1):
			item_location_x = x + dx + selector_width + label_width/2 + (i-1)*(selector_width + label_width + dz)
			item_location_y = y + dy + dz

			font_color = black
			if self.selectable_labels[i-1] == False:
				font_color = gray

			# draw labels 
			message_to_screen(self.labels[i-1], font_color, display_window, item_location_x, item_location_y)