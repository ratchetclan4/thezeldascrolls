import pygame
from tools import *
from Spell import *


#-----------------------------------
#             Item
# ----------------------------------
class Item(pygame.sprite.Sprite):
	def __init__(self, image_filename = None):
		pygame.sprite.Sprite.__init__(self)
		self.image = None
		if image_filename != None:
			self.image = pygame.image.load(image_filename)
		self.frame = 1

		self.container = None
		self.value = None
		self.cost = None


#-----------------------------------
#             Weapons
# ----------------------------------
class Weapons(Item):
	def __init__(self, weapon_image_filename = None):
		super(Weapons, self).__init__(weapon_image_filename)
		self.container = 'Inventory'
		self.slots = 0
		self.att = 0
		self.ap = 0


# Iron weapons
class IronDagger(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(IronDagger, self).__init__(weapon_image_filename)

class IronSword(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(IronSword, self).__init__(weapon_image_filename)

class IronLongsword(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(IronLongsword, self).__init__(weapon_image_filename)

class IronRapier(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(IronRapier, self).__init__(weapon_image_filename)



# Steel weapons
class SteelDagger(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(SteelDagger, self).__init__(weapon_image_filename)

class SteelSword(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(SteelSword, self).__init__(weapon_image_filename)

class SteelLongsword(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(SteelLongsword, self).__init__(weapon_image_filename)

class SteelRapier(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(SteelRapier, self).__init__(weapon_image_filename)



# Dragon weapons
class DragonDagger(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(DragonDagger, self).__init__(weapon_image_filename)

class DragonSword(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(DragonSword, self).__init__(weapon_image_filename)

class DragonLongsword(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(DragonLongsword, self).__init__(weapon_image_filename)

class DragonRapier(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(DragonRapier, self).__init__(weapon_image_filename)


# misc weapons
class Claw(Weapons):
	def __init__(self, weapon_image_filename = None):
		super(Claw, self).__init__(weapon_image_filename)








#-----------------------------------
#              Armour
# ----------------------------------
class Armour(Item):
	def __init__(self, armour_image_filename = None):
		super(Armour, self).__init__(armour_image_filename)
		self.container = 'Inventory'
		self.armour = 0


class Leather(Armour):
	def __init__(self, armour_image_filename = None):
		super(Leather, self).__init__(armour_image_filename)
		self.armour = 4


class Mail(Armour):
	def __init__(self, armour_image_filename = None):
		super(Mail, self).__init__(armour_image_filename)
		self.armour = 6

class Plate(Armour):
	def __init__(self, armour_image_filename = None):
		super(Plate, self).__init__(armour_image_filename)
		self.armour = 8

class DragonHide(Armour):
	def __init__(self, armour_image_filename = None):
		super(DragonHide, self).__init__(armour_image_filename)
		self.armour = 12






#-----------------------------------
#              Potions
# ----------------------------------
class Potion(Item):
	def __init__(self, potion_image_filename = None):
		super(Potion, self).__init__(potion_image_filename)
		self.container = 'Inventory'
		self.attribute = None
		self.value = 0


class MinorHealth(Potion):
	def __init__(self, potion_image_filename = None):
		super(MinorHealth, self).__init__(potion_image_filename)
		self.attribute = 'Health'
		self.value = 5


class MajorHealth(Potion):
	def __init__(self, potion_image_filename = None):
		super(MajorHealth, self).__init__(potion_image_filename)
		self.attribute = 'Health'
		self.value = 10


class MinorMana(Potion):
	def __init__(self, potion_image_filename = None):
		super(MinorMana, self).__init__(potion_image_filename)
		self.attribute = 'Mana'
		self.value = 5


class MajorMana(Potion):
	def __init__(self, potion_image_filename = None):
		super(MajorMana, self).__init__(potion_image_filename)
		self.attribute = 'Mana'
		self.value = 10



#-----------------------------------
#              Scrolls
# ----------------------------------
class Scroll(Item):
	def __init__(self, scroll_image_filename = None):
		super(Scroll, self).__init__(scroll_image_filename)
		self.spell = None
		self.container = 'Spells'
		self.value = 0

	def get_spell(self):
		return self.spell


class ScrollOfFireball(Scroll):
	def __init__(self, scroll_image_filename = None):
		super(ScrollOfFireball, self).__init__(scroll_image_filename)
		self.spell = Fireball("../artwork/fireball.png", (4,4))
		self.attribute = 'Mana'
		self.value = 5


class ScrollOfBlizzard(Scroll):
	def __init__(self, scroll_image_filename = None):
		super(ScrollOfBlizzard, self).__init__(scroll_image_filename)
		self.spell = Blizzard("../artwork/lightning.png", (7,1))
		self.attribute = 'Mana'
		self.value = 5


class ScrollOfWhirlwind(Scroll):
	def __init__(self, scroll_image_filename = None):
		super(ScrollOfWhirlwind, self).__init__(scroll_image_filename)
		self.spell = Whirlwind("../artwork/whirlwind.png", (7,1))
		self.attribute = 'Mana'
		self.value = 5

