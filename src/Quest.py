import pygame
from setup import *



class Quest(object):
	def __init__(self, issuer):
		self.issued_by = issuer
		self.completed = False
		


class TalkToNPC(Quest):
	def __init__(self, issuer):
		super(TalkToNPC, self).__init__(issuer)


class KillEnemy(Quest):
	def __init__(self, issuer):
		super(KillEnemy, self).__init__(issuer)



