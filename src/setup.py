import pygame
import random
import pytmx
from pytmx.util_pygame import load_pygame
from constants import *
from tools import *
from Container import *
from Dialog import *
from Player import *
from Equipment import *
from NPC import *
from Shop import *
from World import *
from Battle import *
from Stats import *
from Start import *
from GameOver import *
from FastTravel import *

# initialize pygame
pygame.init()
clock = pygame.time.Clock() 
random.seed()

# create game display
game_display = pygame.display.set_mode(SCREEN_SIZE)
pygame.display.set_caption("The Zelda Scrolls")

# set intro background image
start_background = pygame.image.load("../artwork/woods.png")
start_background = pygame.transform.scale(start_background, SCREEN_SIZE)


# create player
player_dict = {'Name': 'Zelda Anstett',
			         'Location': (0, 0),
               'Level': 1,
               'Points': 0,
		           'Gold': 120,
               'Health': 5*10,
               'Mana': 23*10,
               'XP': 0,
               'Strength': 10,
               'Agility': 16,
               'Constitution': 5,
               'Wisdom': 12,
               'Intelligence': 23,
               'Defense': 42,
               'Armour': Leather("../artwork/armour.png"),
               'Weapon': IronSword("../artwork/sword.png"),
               'Spells': Container(),
               'Inventory': Container(),
               'Quests': Container(),
               'Cities': Container()
               }
player = Player(player_dict, "../artwork/ch001.png", (3,4))
player.add_city('Goldstream', (5152,9952))
player.add_city('Darkstream', (7808,12352))
player.add_city('Three Rivers', (4704,10112))
player.add_city('Forty Mile', (6112,10496))
player.add_city('Helmhold', (12192,10336))
player.add_city('Winterhelm', (7232, 7296))
player.add_city('Felguard', (9440,8864))
player.add_city('Belguard', (9440, 15712))
player.add_city('Carathas', (8672,10752))






# NPC's 
npc_kevstone = {'Name': 'Professor Kevstone',
               'Location': (0, 0),
               'Level': 10,
               'Points': 0,
               'Gold': 120,
               'Health': 5*10,
               'Mana': 23*10,
               'XP': 0,
               'Strength': 10,
               'Agility': 16,
               'Constitution': 5,
               'Wisdom': 12,
               'Intelligence': 23,
               'Defense': 42,
               'Armour': Leather("../artwork/armour.png"),
               'Weapon': IronSword("../artwork/sword.png"),
               'Spells': Container(),
               'Inventory': Container(),
               'Quests': Container(),
               'Dialog': Dialog("../data/kevstone.xml") 
               }

npc_jeb = {'Name': 'Jeb',
               'Location': (512, 256),
               'Level': 10,
               'Points': 0,
               'Gold': 120,
               'Health': 5*10,
               'Mana': 23*10,
               'XP': 0,
               'Strength': 10,
               'Agility': 16,
               'Constitution': 5,
               'Wisdom': 12,
               'Intelligence': 23,
               'Defense': 42,
               'Armour': Leather("../artwork/armour.png"),
               'Weapon': IronSword("../artwork/sword.png"),
               'Spells': Container(),
               'Inventory': Container(),
               'Quests': Container(),
               'Dialog': Dialog("../data/jeb.xml") 
               }

npc_bob = {'Name': 'Bob',
               'Location': (768, 224),
               'Level': 10,
               'Points': 0,
               'Gold': 120,
               'Health': 5*10,
               'Mana': 23*10,
               'XP': 0,
               'Strength': 10,
               'Agility': 16,
               'Constitution': 5,
               'Wisdom': 12,
               'Intelligence': 23,
               'Defense': 42,
               'Armour': Leather("../artwork/armour.png"),
               'Weapon': IronSword("../artwork/sword.png"),
               'Spells': Container(),
               'Inventory': Container(),
               'Quests': Container(),
               'Dialog': Dialog("../data/bob.xml") 
               }

npc_steve = {'Name': 'Steve',
               'Location': (320, 480),
               'Level': 10,
               'Points': 0,
               'Gold': 120,
               'Health': 5*10,
               'Mana': 23*10,
               'XP': 0,
               'Strength': 10,
               'Agility': 16,
               'Constitution': 5,
               'Wisdom': 12,
               'Intelligence': 23,
               'Defense': 42,
               'Armour': Leather("../artwork/armour.png"),
               'Weapon': IronSword("../artwork/sword.png"),
               'Spells': Container(),
               'Inventory': Container(),
               'Quests': Container(),
               'Dialog': Dialog("../data/steve.xml") 
               }

enemy_red_dragon = {'Name': 'Red Dragon',
               'Location': (704, 704),
               'Level': 10,
               'Points': 0,
               'Gold': 120,
               'Health': 5*10,
               'Mana': 23*10,
               'XP': 0,
               'Strength': 10,
               'Agility': 16,
               'Constitution': 5,
               'Wisdom': 12,
               'Intelligence': 23,
               'Defense': 42,
               'Armour': DragonHide("../artwork/armour.png"),
               'Weapon': Claw("../artwork/claw.png"),
               'Inventory': Container(),
               'Quests': Container(),
               }




# Goldstream
goldstream_tavern_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
goldstream_tavern_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


goldstream_armour_shop_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
goldstream_armour_shop_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


goldstream_weapon_shop_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
goldstream_weapon_shop_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


goldstream_potion_shop_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
goldstream_potion_shop_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


goldstream_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
goldstream_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

goldstream_house_characters = {}
goldstream_house_components = {}
goldstream_house_map = load_pygame("../maps/house.tmx")


goldstream_characters = {'Jeb': NPC(npc_jeb, "../artwork/ch002.png", (3,4), pygame.Rect(288, 192, 256, 128)),
                    'Bob': NPC(npc_bob, "../artwork/ch003.png", (3,4), pygame.Rect(608, 192, 160, 96)),
                    'Steve': NPC(npc_steve, "../artwork/ch004.png", (3,4), pygame.Rect(320, 480, 192, 160)),
                    'Red Dragon': Enemy(enemy_red_dragon, "../artwork/ch005.png", (3,4), pygame.Rect(704, 704, 192, 96))}
goldstream_components = {'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}
goldstream_map = load_pygame("../maps/goldstream.tmx")  # load map



# Darkstream
darkstream_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
darkstream_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

darkstream_characters = {}
darkstream_components = {'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}
darkstream_map = load_pygame("../maps/darkstream.tmx")  # load map



# Northstream
northstream_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
northstream_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

northstream_characters = {}
northstream_components = {'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}
northstream_map = load_pygame("../maps/northstream.tmx")  # load map




# Three Rivers
threerivers_tavern_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
threerivers_tavern_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


threerivers_armour_shop_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
threerivers_armour_shop_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


threerivers_weapon_shop_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
threerivers_weapon_shop_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


threerivers_potion_shop_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
threerivers_potion_shop_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


threerivers_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
threerivers_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

threerivers_characters = {}
threerivers_components = {}
threerivers_map = load_pygame("../maps/threerivers.tmx")  # load map




# Forty Mile
fortymile_tavern_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
fortymile_tavern_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


fortymile_armour_shop_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
fortymile_armour_shop_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


fortymile_weapon_shop_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
fortymile_weapon_shop_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


fortymile_potion_shop_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
fortymile_potion_shop_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}


fortymile_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
fortymile_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

fortymile_characters = {}
fortymile_components = {}
fortymile_map = load_pygame("../maps/fortymile.tmx")  # load map



# Helmhold
helmhold_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
helmhold_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

helmhold_characters = {}
helmhold_components = {'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}
helmhold_map = load_pygame("../maps/helmhold.tmx")  # load map



# Winterhelm
winterhelm_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
winterhelm_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

winterhelm_characters = {}
winterhelm_components = {'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}
winterhelm_map = load_pygame("../maps/winterhelm.tmx")  # load map



# Felguard
felguard_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
felguard_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

felguard_characters = {}
felguard_components = {'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}
felguard_map = load_pygame("../maps/felguard.tmx")  # load map



# Belguard
belguard_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
belguard_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

belguard_characters = {}
belguard_components = {'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}
belguard_map = load_pygame("../maps/belguard.tmx")  # load map



# Carathas
carathas_wharf_characters = {'Kevstone': NPC(npc_kevstone, "../artwork/ch002.png", (3,4))}
carathas_wharf_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                     'GoldBox': MessageBox("../artwork/scroll.png"),
                     'MessageBox': MessageBox("../artwork/scroll.png"),
                     'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}

carathas_characters = {}
carathas_components = {'DialogBox': DialogBox("../artwork/sword.png", "../artwork/scroll.png")}
carathas_map = load_pygame("../maps/carathas.tmx")  # load map






# Overworld
start_characters = {}
start_components = {'Menu': Menu("../artwork/sword.png", "../artwork/scroll.png")}

gameover_characters = {}
gameover_components = {}

stats_characters = {}
stats_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                    'DisplayMenu': Menu("../artwork/sword.png", "../artwork/scroll.png", 10),
                    'Popup': BinaryInfoMenu("../artwork/sword.png", "../artwork/scroll.png"),
                    'GoldBox': MessageBox("../artwork/scroll.png"),
                    'LevelBox': MessageBox("../artwork/scroll.png"),
                    'PointsBox': MessageBox("../artwork/scroll.png"),
                    'SurfaceBox': SurfaceBox("../artwork/scroll.png")}


fasttravel_characters = {}
fasttravel_components = {}


battle_characters = {}
battle_components = {'MainMenu': Menu("../artwork/sword.png", "../artwork/scroll.png"),
                    'Popup': Menu("../artwork/sword.png", "../artwork/scroll.png", 3),
                    'SpriteMenu': SpriteMenu("../artwork/sword.png"),
                    'MessageBox': MessageBox("../artwork/scroll.png"),
                    'SurfaceBox': SurfaceBox("../artwork/scroll.png")}

overworld_characters = {}
overworld_components = {}
overworld_map = load_pygame("../maps/overworld.tmx")



game_states = {'Start': Start(start_characters, start_components, None, 'Start', 'Overworld'),
                'Game Over': GameOver(gameover_characters, gameover_components, None, 'Game Over', 'Start'),
                'Stats': Stats(stats_characters, stats_components, None, 'Stats', None),
                'FastTravel': FastTravel(fasttravel_characters, fasttravel_components, None, 'FastTravel', 'Overworld'),
                'Overworld': World(overworld_characters, overworld_components, overworld_map, (161*32, 311*32), None, 'Overworld', None),
                'Battle': Battle(battle_characters, battle_components, 'Overworld', 'Battle', None),
                'Forty Mile': World(fortymile_characters, fortymile_components, fortymile_map, (32, 192), 'Overworld', 'Forty Mile', None),
                'Forty Mile Tavern': TavernShop(fortymile_tavern_characters, fortymile_tavern_components, 'Forty Mile', 'Forty Mile Tavern', None),
                'Forty Mile Armour Shop': ArmourShop(fortymile_armour_shop_characters, fortymile_armour_shop_components, 'Forty Mile', 'Forty Mile Armour Shop', None),
                'Forty Mile Weapon Shop': WeaponShop(fortymile_weapon_shop_characters, fortymile_weapon_shop_components, 'Forty Mile', 'Forty Mile Weapon Shop', None),
                'Forty Mile Potion Shop': PotionShop(fortymile_potion_shop_characters, fortymile_potion_shop_components, 'Forty Mile', 'Forty Mile Potion Shop', None),
                'Forty Mile Wharf': Wharf(fortymile_wharf_characters, fortymile_wharf_components, 'Forty Mile', 'Forty Mile Wharf', None),
                'Three Rivers': World(threerivers_characters, threerivers_components, threerivers_map, (32, 192), 'Overworld', 'Three Rivers', None),
                'Three Rivers Tavern': TavernShop(threerivers_tavern_characters, threerivers_tavern_components, 'Three Rivers', 'Three Rivers Tavern', None),
                'Three Rivers Armour Shop': ArmourShop(threerivers_armour_shop_characters, threerivers_armour_shop_components, 'Three Rivers', 'Three Rivers Armour Shop', None),
                'Three Rivers Weapon Shop': WeaponShop(threerivers_weapon_shop_characters, threerivers_weapon_shop_components, 'Three Rivers', 'Three Rivers Weapon Shop', None),
                'Three Rivers Potion Shop': PotionShop(threerivers_potion_shop_characters, threerivers_potion_shop_components, 'Three Rivers', 'Three Rivers Potion Shop', None),
                'Three Rivers Wharf': Wharf(threerivers_wharf_characters, threerivers_wharf_components, 'Three Rivers', 'Three Rivers Wharf', None),
                'Goldstream': World(goldstream_characters, goldstream_components, goldstream_map, (32, 192), 'Overworld', 'Goldstream', None),
                'Goldstream Tavern': TavernShop(goldstream_tavern_characters, goldstream_tavern_components, 'Goldstream', 'Goldstream Tavern', None),
                'Goldstream Armour Shop': ArmourShop(goldstream_armour_shop_characters, goldstream_armour_shop_components, 'Goldstream', 'Goldstream Armour Shop', None),
                'Goldstream Weapon Shop': WeaponShop(goldstream_weapon_shop_characters, goldstream_weapon_shop_components, 'Goldstream', 'Goldstream Weapon Shop', None),
                'Goldstream Potion Shop': PotionShop(goldstream_potion_shop_characters, goldstream_potion_shop_components, 'Goldstream', 'Goldstream Potion Shop', None),
                'Goldstream Wharf': Wharf(goldstream_wharf_characters, goldstream_wharf_components, 'Goldstream', 'Goldstream Wharf', None),
                'Goldstream House': World(goldstream_house_characters, goldstream_house_components, goldstream_house_map, (320,352), 'Goldstream', 'Goldstream House', None),
                'Darkstream': World(darkstream_characters, darkstream_components, darkstream_map, (32, 192), 'Overworld', 'Darkstream', None),
                'Darkstream Wharf': Wharf(darkstream_wharf_characters, darkstream_wharf_components, 'Darkstream', 'Darkstream Wharf', None),
                'Helmhold': World(helmhold_characters, helmhold_components, helmhold_map, (32, 192), 'Overworld', 'Helmhold', None),
                'Helmhold Wharf': Wharf(helmhold_wharf_characters, helmhold_wharf_components, 'Helmhold', 'Helmhold Wharf', None),
                'Winterhelm': World(winterhelm_characters, winterhelm_components, winterhelm_map, (32, 192), 'Overworld', 'Winterhelm', None),
                'Winterhelm Wharf': Wharf(winterhelm_wharf_characters, winterhelm_wharf_components, 'Winterhelm', 'Winterhelm Wharf', None),
                'Felguard': World(felguard_characters, felguard_components, felguard_map, (32, 192), 'Overworld', 'Felguard', None),
                'Felguard Wharf': Wharf(felguard_wharf_characters, felguard_wharf_components, 'Felguard', 'Felguard Wharf', None),
                'Belguard': World(belguard_characters, belguard_components, belguard_map, (32, 192), 'Overworld', 'Belguard', None),
                'Belguard Wharf': Wharf(belguard_wharf_characters, belguard_wharf_components, 'Belguard', 'Belguard Wharf', None),
                'Carathas': World(carathas_characters, carathas_components, carathas_map, (32, 192), 'Overworld', 'Carathas', None),
                'Carathas Wharf': Wharf(carathas_wharf_characters, carathas_wharf_components, 'Carathas', 'Carathas Wharf', None)}