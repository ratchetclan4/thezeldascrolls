import pygame
from constants import *
from tools import *


class Spell(pygame.sprite.Sprite):
	def __init__(self, image_tile_filename, image_tile_size):
		pygame.sprite.Sprite.__init__(self)
		self.image_tile_size = image_tile_size
		self.image_tile = SpriteSheet(image_tile_filename)
		self.image = self.image_tile.image_at((0, 0, 32, 32))
		self.image_dict = {}
		self.image_index = 0
		self.frame = 1
		self.rect = self.image.get_rect()
		self.rect.x = 0
		self.rect.y = 0
		self.speed = 1
		self.x_vel = 0
		self.y_vel = 0
		self.state = 'resting'  # can be moving or resting
		self.direction = 'right'  # direction of movement, i.e. left, right, up, down or at rest
		self.velocities = {'left': (-4,0), 'right': (4,0), 'up': (0,-4), 'down': (0,4)}

		self.make_image_dictionary()

	# make dictionary of images belonging to character sprite
	def make_image_dictionary(self):
		down = []; left = []; right = []; up = [];
		for i in range(0, self.image_tile_size[0]):
			if self.image_tile_size[1] == 1:
				right.append(self.image_tile.image_at((32*i, 0, 32, 32)))
			elif self.image_tile_size[1] >= 4:
				down.append(self.image_tile.image_at((32*i, 0, 32, 32)))
				left.append(self.image_tile.image_at((32*i, 32, 32, 32)))
				right.append(self.image_tile.image_at((32*i, 64, 32, 32)))
				up.append(self.image_tile.image_at((32*i, 96, 32, 32)))
		self.image_dict = {'down': down, 'up': up, 'left': left, 'right': right}

	def make_image_dictionary(self):
		right_images = []
		for i in range(0, self.image_tile_size[0]):
			right_images.append(self.image_tile.image_at((32*i, 0, 32, 32)))
		self.image_dict = {'right': right_images}

	# methods to reposition, rescale, redirect, and restate character sprite
	def reposition(self, location):
		self.rect.x = location[0]
		self.rect.y = location[1]
		self.image_index = 0

	def rescale(self, scale):
		self.image = pygame.transform.scale(self.image, scale)

	def redirect(self, direction):
		self.direction = direction

	def restate(self, state):
		self.state = state

	# changes current sprite image based on sprite state and movement direction
	def animation(self):
		if self.state == 'resting' and self.direction == 'rest':
			self.image = self.image_dict[self.direction][0]
			self.image_index = 0
			self.frame = 1
		else:
			if self.frame == 1:
				self.image = self.image_dict[self.direction][self.image_index]
				self.image_index = (self.image_index+1) % self.image_tile_size[0]
			elif self.frame == FPS/self.image_tile_size[0]:
				self.frame = 0
			self.frame += 1

	# draw sprite at current position
	def draw(self, display):
		display.blit(self.image, (self.rect.x, self.rect.y))








class Fireball(Spell):
	def __init__(self, spell_image_filename, image_tile_size):
		super(Fireball, self).__init__(spell_image_filename, image_tile_size)

	def animate_battle(self, flag):
		if flag == "spell":
			self.reposition((400,200))
			flag = "cast"
		elif flag == "cast":
			self.redirect('right')
			if self.rect.x < 600:
				self.x_vel = self.velocities['right'][0]
			else:
				self.x_vel = 0
				flag = "no spell"
		else:
			pass

		self.rect.x += self.x_vel		
		self.animation()
		self.rescale((128, 128))
		return flag




class Blizzard(Spell):
	def __init__(self, spell_image_filename, image_tile_size):
		super(Blizzard, self).__init__(spell_image_filename, image_tile_size)

	def animate_battle(self, flag):
		if flag == "spell":
			self.reposition((400,200))
			flag = "cast"
		elif flag == "cast":
			self.redirect('right')
			if self.rect.x < 600:
				self.x_vel = self.velocities['right'][0]
			else:
				self.x_vel = 0
				flag = "no spell"
		else:
			pass

		self.rect.x += self.x_vel		
		self.animation()
		self.rescale((128, 128))
		return flag




class Whirlwind(Spell):
	def __init__(self, spell_image_filename, image_tile_size):
		super(Whirlwind, self).__init__(spell_image_filename, image_tile_size)

	def animate_battle(self, flag):
		if flag == "spell":
			self.reposition((400,200))
			flag = "cast"
		elif flag == "cast":
			self.redirect('right')
			if self.rect.x < 600:
				self.x_vel = self.velocities['right'][0]
			else:
				self.x_vel = 0
				flag = "no spell"
		else:
			pass

		self.rect.x += self.x_vel		
		self.animation()
		self.rescale((128, 128))
		return flag