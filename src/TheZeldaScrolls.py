#!/usr/bin/env python
__author__ = 'jsandham'

"""This is a fantasy RPG game about a young girl named Zelda on a
quest to find the golden goose...and more"""
    
import sys
sys.path.insert(0, '/home/james/Documents/thezeldascrolls/src/states')
sys.path.insert(0, '/home/james/Documents/thezeldascrolls/src/characters')
sys.path.insert(0, '/home/james/Documents/thezeldascrolls/src/components')

import pygame
# import main
from main import *


if __name__ =='__main__':
    # setup.GAME
    main()
    pygame.quit()
    sys.exit()
