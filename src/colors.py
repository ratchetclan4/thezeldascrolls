import pygame

# colours
white = pygame.Color(255,255,255)
black = pygame.Color(0,0,0)
gray = pygame.Color(90,90,90)
red = pygame.Color(153,0,0)
green = pygame.Color(0,255,0)
blue = pygame.Color(0,0,153)
gold = pygame.Color(255,215,0)
purple = pygame.Color(128,0,128)