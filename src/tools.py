import pygame
import math
from colors import *
from fonts import *


# some helping funtions
def text_objects(text,color,size):
	if size == "small":
		textSurface = small_font.render(text,True,color)
	elif size == "medium":
		textSurface = medium_font.render(text,True,color)
	elif size == "large":
		textSurface = large_font.render(text,True,color)
	return textSurface, textSurface.get_rect()


def message_to_screen(msg, color, display, x, y, size="small"):
	textSurf, textRect = text_objects(msg,color,size)
	textRect.center = x, y
 	display.blit(textSurf, textRect)


def image_to_screen(filename, display, x, y):
	image = pygame.image.load(filename)
	display.blit(image, (x,y))


class TextSurface(pygame.Surface):
  	def __init__(self, text, font, font_size, color, width, height = 100):
	    super(TextSurface, self).__init__((width, height), pygame.SRCALPHA)
	    self.text = text
	    self.font = font
	    self.font_size = font_size
	    self.color = color
	    self.textSize = pygame.font.SysFont(self.font, self.font_size).size(self.text)
	    self.charWidth = int(math.ceil(self.textSize[0] / float(len(text))))
	    self.charHeight = self.textSize[1]
	    self.numberOfLines = self.textSize[0] / width + 1
	    self.numberOfCharPerLine = width / self.charWidth

	    start = 0
	    end = min(len(text), self.numberOfCharPerLine)
	    for i in range(0, self.numberOfLines+1):
	      	while end > start:
	        	if text[end-1] == " " or end == len(text):
	          		break
	        	else:
	          		end = end - 1

	      	substring = text[start:end]
	      	tempSurface = pygame.font.SysFont(self.font, self.font_size).render(substring, True, self.color)
	      	self.blit(tempSurface, (0, self.charHeight*i))
	      	start = end
	      	end = start + min(len(text[start:]), self.numberOfCharPerLine)




class Bar(pygame.Surface):
	def __init__(self, width, height, val, max_val, color, border_thickness = 2):
		super(Bar, self).__init__((width, height), pygame.SRCALPHA)
		bar_width = width - border_thickness
		bar_height = height - border_thickness
		bar_width = int(val*bar_width / float(max_val))

		background_surface = pygame.Surface((width, height))
		background_surface.fill(black)

		bar_surface = pygame.Surface((bar_width, bar_height))
		bar_surface.fill(color)

		text = TextSurface(str(val)+"/"+str(max_val), "freesans", 15, white, width, height)

		self.blit(background_surface, (0,0))
		self.blit(bar_surface, (border_thickness/2, border_thickness/2))
		self.blit(text, (border_thickness/2, border_thickness/2))

		# pygame.draw.rect(self, black, pygame.Rect((0,0), (width, height)))
		# pygame.draw.rect(self, self.color, pygame.Rect((self.border_thickness/2, self.border_thickness/2), (sub_width, sub_height)))
		
		



class SpriteSheet(object):
	def __init__(self, filename):
		try:
			self.sheet = pygame.image.load(filename)
		except pygame.error, message:
			print 'Unable to load spritesheet image:', filename
			raise SystemExit, message


	def image_at(self, rectangle):
		rect = pygame.Rect(rectangle)
		image = pygame.Surface(rect.size, pygame.SRCALPHA, 32)
		image = image.convert_alpha()
		image.blit(self.sheet, (0,0), rect)
		return image


	def get_image(self):
		rect = self.sheet.get_rect()
		image = pygame.Surface(rect.size, pygame.SRCALPHA, 32)
		image = image.convert_alpha()
		image.blit(self.sheet, (0,0), rect)
		return image




class Block(pygame.sprite.Sprite):
	def __init__(self, x, y, width, height):
		pygame.sprite.Sprite.__init__(self)
		self.global_rect = pygame.Rect(x, y, width, height)
		self.rect = pygame.Rect(x, y, width, height)
