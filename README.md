Project STATUS: not complete


This is a fantasy RPG game about a young girl named Zelda on a quest to find the golden goose...and more

To run this game you need python2, pygame, and pytmx. On ubuntu python2 is most likely already installed. To install pygame and pytmx simply type:

sudo apt-get update  
sudo apt-get install python-pygame  
sudo apt install python-pip  
pip install pytmx  

To run the game script type (in /src directory): 

./TheZeldaScrolls.sh  

in a terminal.
